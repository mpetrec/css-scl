%This function performs reachability reduction of an ALPV system. 
%%Inputs: D=n_p, number of linear submodels, n - number of continous states,
%p-number of outputs, m- number of inputs.
% Anum=[A0;A1;A2;..;AD], Cnum=[C0;C1;..;CD] (rows one under the other),
% [B0,B1,...,BD] (columns stacked together).  
% x0 -- initial state
%The
%resulting transformation matrix is stored in Reach_mat and 
%the matrices of the reduced system are stored in Ar,Br,Cr,x0r, according
%to the same principle as the input is stored; red_ord_r is the order of
%the reduced system.

function [Reach_mat,red_ord_r,Ar,Br,Cr,x0r]=ReachRedLPV(D,n,m,p,Anum,Bnum,Cnum,x_0)

    B_hat=[x_0, Bnum];

      V_f=orth(B_hat);
      V_0=V_f

      quit = 0;
      r=rank(V_f);
      while (quit == 0)
          r=rank(V_f);
          V_prime = V_0;
          for j=1:D
              V_prime=[V_prime, Anum((j-1)*n+1:j*n,:)*V_f];
          end
          V_f=orth(V_prime);
          quit = (r == rank(V_f));
      end
    
    
    Reach_mat = V_f;
    
    red_ord_r = r;
    
    Ar = zeros(D*red_ord_r,red_ord_r);
    Br = zeros(D*red_ord_r,m);
    Cr = zeros(p,D*red_ord_r);
    
    V_inv=V_f';
    x0r=V_inv*x_0;

    
    Cr=Cnum*V_f;

    Br=V_inv*Bnum;

    for q=1:D
        Ar((q-1)*red_ord_r+1:q*red_ord_r,:)=V_inv*Anum((q-1)*n+1:q*n,:)*V_f;
    end
    



end