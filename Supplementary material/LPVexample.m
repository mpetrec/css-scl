p=sym('p')

%Definition of the system from Example 2
A0=[0 1 -1
    -1 0 1
    -1 1 0]'
A1=[1 -1 -1
    -1 2 0
    -1 0 2]'
A0=A0+A1
C0=[1
    0
    0]'
C1=[0
    1
    1]'
B0=[1 -1 -1]'
B1=[2 -2 -2]'

Ap=A0+p*A1
Bp=B0+p*B1
Cp=C0+p*C1

%pi=\xi^i p
p1=sym('p1')
p2=sym('p2')
p3=sym('p3')
p4=sym('p4')

%Observability matrix (O diamond p) of  \Sigma viewed the meromorphic LVP-SS
Op=[Cp;subs(Cp,p,p1)*Ap;subs(Cp,p,p2)*subs(Ap,p,p1)*Ap]

tildeC=[C0;C1]

%Observability matrix of the LPV-SSA as in Section III
O=[tildeC;tildeC*A0;tildeC*A1;tildeC*A0*A0;tildeC*A1*A0;tildeC*A0*A1;tildeC*A1*A1]

%Observability reduction procedure (Procedure 1) of the paper
Z=eye(3)
T=inv([Z(:,1:2),null(O)])  %basis transformation B

%Matrices in the new basis
Att0=T*A0*inv(T)  
Att1=T*A1*inv(T)
Btt0=T*B0
Btt1=T*B1
Ctt0=C0*inv(T)
Ctt1=C1*inv(T)


%%Matrices of the reduced (observable) subsystem. 
Am0=Att0(1:2,1:2)  %A^m_0
Am1=Att1(1:2,1:2)  %A^m_1
Bm0=Btt0(1:2,:)    %B^m_0
Bm1=Btt1(1:2,:)    %B^m_1
Cm0=Ctt0(:,1:2)    %C^m_0
Cm1=Ctt1(:,1:2)    %C^m_1



%Observability reduction for \Sigma viewed as meromorphic LPV-SS
Tpinv=[colspace(Op),null(Op)]
Tp=inv([colspace(Op),null(Op)]) %matrix (T \diamond p)

 save('T_diamond_p.mat', 'Tp')


%Applying Tp to \Sigma
Tpn=subs(Tp,{p,p1,p2},{p1,p2,p3})

Apt=Tpn*Ap*Tpinv
Bpt=Tpn*Bp
Cpt=Cp*Tpinv

Apo=Apt(1:2,1:2) %(A^o \diamond p)

save('A^o_diamond_p.mat', 'Apo')

Bpo=Bpt(1:2,:)   %(B^o \diamond p)

save('B^o_diamond_p.mat','Bpo')
Cpo=Cpt(:,1:2)   %(C^o \diamond p)

save('C^o_diamond_p.mat','Cpo')


%Finding the state-space transormation from \Sigma^o to \Sigma^m
Aopn=Ao0+Ao1*p
Bopn=Bo0+Bo1*p
Copn=Co0+Co1*p

Op1=[Copn;subs(Copn,p,p1)*Aopn]  %Observability matrix of \Sigma^m as a meromorphic LPV-SS
Op2=[Cpo;subs(Cpo,{p,p1,p2,p3},{p1,p2,p3,p4})*Apo] %Observability matrix of \Sigma^o as a meromorphic LPV-SS

TT=inv(Op1)*Op2  %State-space transformation

save('Transformation_from_Sigma^o_to_Sigma^m.mat','TT')

%Checking that TT is a transformation
p5=sym('p5')
TTn=subs(TT,{p,p1,p2,p3,p4},{p1,p2,p3,p4,p5}) %TT^leftrightarrow


disp('The three matrices below should be zero')
simplify(TTn*Apo*inv(TT)-Aopn) % should be zero
simplify(TTn*Bpo-Bopn)
simplify(Cpo*inv(TT)-Copn)
