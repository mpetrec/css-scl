%This function performs observability reduction of an ALPV system. 
%%Inputs: D=n_p, number of linear submodels, n - number of continous states,
%p-number of outputs, m - number of inputs.
% Anum=[A0;A1;A2;..;AD], Cnum=[C0;C1;..;CD] (rows one under the other),
% [B0,B1,...,BD] (columns stacked together).  
% x0 -- initial state
%The
%resulting transformation matrix is stored in Wf and 
%the matrices of the reduced system are stored in Ao,Bo,Co,xo0, according
%to the same principle as the input is stored; red_ord_o is the order of
%the reduced system.

function [Wf,red_ord_o,Ao,Bo,Co,xo0]=ObsRedLPV(D,n,m,p,Anum,Bnum,Cnum,x0)
    Wf=orth(Cnum');
    quit = 0;
    r=rank(Wf);
    while (quit == 0)
       Wprime=Cnum';
       r = rank(Wf);
       for j=1:D
          Wprime=[Wprime, Anum((j-1)*n+1:j*n,:)'*Wf];
       end
       Wf=orth(Wprime);
       quit = (r == rank(Wf));    
    end
    red_ord_o=r;
    Ao=zeros(D*red_ord_o,red_ord_o);
    xo0=Wf'*x0;
    Co=Cnum*Wf;
    Bo=Wf'*Bnum;
    for q=1:D
        Ao((q-1)*red_ord_o+1:q*red_ord_o,:)=Wf'*Anum((q-1)*n+1:q*n,:)*Wf;
    end
    
end
